'use strict';

angular.module('myApp.view2', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view2', {
    templateUrl: 'view2/view2.html',
    controller: 'View2Ctrl'
  });
}])

.controller('View2Ctrl', function($scope, $http) {

  $scope.newOne = {
    title: '',
    description: '',
    contact: ''
  }
  var url = 'http://80.211.154.28:8080/api/offers/refunds';

  $scope.getRefunds = function () {
    $http({
      method: 'GET',
      url: url
    })
    .then(function(response) {
      console.log('view2');
        console.log(response);
    }, function(response) {
      console.log('view2');
      console.log('ERROR');
      console.log(response);
    });
  };

  $scope.getRefunds();

});
