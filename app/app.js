'use strict';

// Declare app level module which depends on views, and components
var app = angular.module('myApp', [
  'ngMaterial',
  'ngSanitize'
]);
app.controller('myCtrl', function($scope, $http, $mdDialog) {

  $scope.clickOffer = function () {
    $scope.flags.isSelected = true;
    $scope.flags.isOfferSelected = true;
  };

  $scope.clickSearch = function () {
    $scope.getItems();
    $scope.flags.isSelected = true;
    $scope.flags.isSearchSelected = true;
  };

  $scope.clickCancel = function () {
    $scope.reset();
  };

  $scope.clickSubmit = function () {
    if ($scope.checkMandatory()) {
      $scope.postData();
      $scope.reset();
      $scope.flags.isSelected = true;
      $scope.flags.isOfferSelected = true;
      $scope.sayThanks();
    } else {
      $scope.flags.missingMandatory = true;
      $scope.showAlert();
    }
  };

  $scope.reset = function () {
    $scope.flags = {
      isSelected: false,
      isOfferSelected: false,
      isSearchSelected: false,
      missingMandatory: false
    };

    $scope.newItem = {
      title: '',
      description: '',
      contact: '',
      selectedRefund: ''
    };

    $scope.filtering = {
      category: '',
      refund: ''
    };
  };
  $scope.reset();






  $scope.flags = {
    isSelected: false,
    isOfferSelected: false,
    isSearchSelected: false,
    missingMandatory: false
  };

  $scope.newItem = {
    title: '',
    description: '',
    contact: '',
    selectedRefund: '',
    selectedCategory: ''
  }

  $scope.checkMandatory = function () {
    if ($scope.newItem.title != '' && $scope.newItem.description != '' && $scope.newItem.contact != '' && $scope.newItem.selectedCategory != '' && $scope.newItem.selectedRefund != '')
      return true;
  };





  var refundsUrl = 'http://80.211.154.28:8080/api/offers/refunds';
  $scope.getRefunds = function () {
    $http({
      method: 'GET',
      url: refundsUrl
    })
    .then(function(response) {
        console.log(response);
        $scope.refunds = response.data.slice();
    }, function(response) {
      console.log('ERROR');
      console.log(response);
    });
  };
  $scope.getRefunds();

  var categoriesUrl = 'http://80.211.154.28:8080/api/offers/categories';
  $scope.getCategories = function () {
    $http({
      method: 'GET',
      url: categoriesUrl
    })
    .then(function(response) {
        console.log(response);
        $scope.categories = response.data.slice();
    }, function(response) {
      console.log('ERROR');
      console.log(response);
    });
  };
  $scope.getCategories();

  var itemsUrl = 'http://80.211.154.28:8080/api/offers';
  $scope.getItems = function () {
    $http({
      method: 'GET',
      url: itemsUrl
    })
    .then(function(response) {
        console.log(response);
        $scope.items = response.data.slice();
        //$scope.configHeader = response.config.headers;
    }, function(response) {
        console.log(response);
    });
  };

  $scope.categoryIcons = {
    GARDENING: '<i class="fa fa-leaf" aria-hidden="true"></i>',
    BABYSITTING: '<i class="fa fa-child" aria-hidden="true"></i>',
    RENOVATION: '<i class="fa fa-wrench" aria-hidden="true"></i>',
    COOKING: '<i class="fa fa-cutlery" aria-hidden="true"></i>',
    SHOPPING: '<i class="fa fa-shopping-cart" aria-hidden="true"></i>',
    TRANSPORTATION: '<i class="fa fa-car" aria-hidden="true"></i>',
    CRAFTSMANSHIP: '<i class="fa fa-handshake-o" aria-hidden="true"></i>',
    CLEANING: '<i class="fa fa-magic" aria-hidden="true"></i>',
    OTHER: '<i class="fa fa-smile-o" aria-hidden="true"></i>',
    FOR_WOMAN: '<i class="fa fa-female" aria-hidden="true"></i>',
    FOR_MEN: '<i class="fa fa-male" aria-hidden="true"></i>',
    LESSONS: '<i class="fa fa-graduation-cap" aria-hidden="true"></i>',
    SKILLS_EXCHANGE: '<i class="fa fa-handshake-o" aria-hidden="true"></i>',
    ANIMALS_HELP: '<i class="fa fa-paw" aria-hidden="true"></i>'
  };

  $scope.refundsIcons = {
    FOOD: '<i class="fa fa-cutlery" aria-hidden="true"></i>',
    CLOTHES: '<i class="fa fa-shopping-bag" aria-hidden="true"></i>',
    ACCOMMODATION: '<i class="fa fa-bed" aria-hidden="true"></i>',
    DEVICES: '<i class="fa fa-fax" aria-hidden="true"></i>',
    FURNITURES: '<i class="fa fa-building-o" aria-hidden="true"></i>',
    TOOLS: '<i class="fa fa-wrench" aria-hidden="true"></i>',
    BABY_ACCESSORIES: '<i class="fa fa-child" aria-hidden="true"></i>',
    TICKETS: '<i class="fa fa-ticket" aria-hidden="true"></i>',
    COMPANIONSHIP: '<i class="fa fa-users" aria-hidden="true"></i>',
    DRUGS: '<i class="fa fa-eyedropper" aria-hidden="true"></i>',
    TOYS: '<i class="fa fa-futbol-o" aria-hidden="true"></i>',
    OTHER: '<i class="fa fa-smile-o" aria-hidden="true"></i>',
    SKILLS_EXCHANGE: '<i class="fa fa-handshake-o" aria-hidden="true"></i>',
    BOOKS: '<i class="fa fa-book" aria-hidden="true"></i>',
    ELECTRICAL_APPLIANCES: '<i class="fa fa-television" aria-hidden="true"></i>',
    HOME_ACCESSORIES: '<i class="fa fa-coffee" aria-hidden="true"></i>'
  };





  $scope.showAlert = function() {
    // Appending dialog to document.body to cover sidenav in docs app
    // Modal dialogs should fully cover application
    // to prevent interaction outside of dialog
    $mdDialog.show(
      $mdDialog.alert()
        .clickOutsideToClose(true)
        .title('Attention!')
        .textContent('You need fill all fields.')
        .ariaLabel('Alert Dialog Demo')
        .ok('Got it!')
    );
  };

  $scope.sayThanks = function() {
    // Appending dialog to document.body to cover sidenav in docs app
    // Modal dialogs should fully cover application
    // to prevent interaction outside of dialog
    $mdDialog.show(
      $mdDialog.alert()
        .clickOutsideToClose(true)
        .title('Thank you!')
        .textContent('Thank you for helping each others!')
        .ariaLabel('Alert Dialog Demo')
        .ok('Got it!')
    );
  };


  var postUrl = 'http://80.211.154.28:8080/api/offers';
  $scope.postData = function () {
    $http({
      method: 'POST',
      url: postUrl,
      headers: {
       'Content-Type': 'application/json'
      },
      data: {
        'contactData' : $scope.newItem.contact,
        'description' : $scope.newItem.description,
        'category' : $scope.newItem.selectedCategory,
        'refund' : $scope.newItem.selectedRefund,
        'title' : $scope.newItem.title,
        'image' : ''
      }
    })
    .then(function() {
        console.log('YES');
    }, function(response) {
      console.log(response);
        console.log('NO');
    });
  };



});
